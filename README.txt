 While running a local MYSQL server you can build the database using the following SQL script:

CREATE TABLE `listings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `address` text,
  `contact_details` text,
  `price_per_month` decimal(10,0) DEFAULT NULL,
  `description` text,
  `postcode` varchar(10) DEFAULT NULL,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `listings` (`id`, `address`, `contact_details`, `price_per_month`, `description`, `postcode`, `image`)
VALUES
	(3, '44 MadeUpAvenue, Edinburgh, United Kingdom', 'seller@hey.com', 2220, 'Spacious 3 bedroom house with 2 family rooms, large kitchen and outdoor pool.', 'EH17 2ST', 'https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'),
	(4, '22 Baker St, London, England', 'johnsmith@bt.com', 900, 'Large family home consisting of two bedrooms, one large family bathroom and a games room.', 'EH16 6TH', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/suburban-house-royalty-free-image-1584972559.jpg?resize=980:*'),
	(6, '125 St Andrews Terrace, Edinburgh', 'david@napier.com', 3240, 'Large old property with 2 acres of land. Property built in the 1950\'s with an excellent view over the coast.', 'EH17 772', 'https://a0.muscache.com/im/pictures/1972d80d-caa0-4103-b176-72b52e0283ef.jpg?im_w=960');



In terminal (OSX) or command Prompt (Windows) locate the api folder and run “npm install” to install all the API’s dependencies.

Then run the API locally using node “app.js”.