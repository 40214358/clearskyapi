// load app server using express...
const express = require("express");
const app = express();
const morgan = require("morgan");
const mysql = require("mysql");
const cors = require("cors");

app.use(morgan("short"));
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// connect to database
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password1",
  database: "PropertyDatabase",
});

// fetch listings of interest
app.get("/listings/:id", (req, res) => {
  console.log(
    "Fetching listing enquires made by user with id: " + req.params.id
  );
  const userid = req.params.id;
  const queryString = "SELECT * FROM listings WHERE userId = ?";
  connection.query(queryString, [userid], (err, rows, fields) => {
    if (err) {
      console.log("Failed to fetch user enquiries: " + err);
      res.sendStatus(500);
      res.end();
      return;
    }
    console.log("Successfully fetched user enquires...");
    res.json(rows);
  });
});

// fetch listings
app.get("/listings", (req, res) => {
  console.log("Fetching listings...");
  const queryString = "SELECT * FROM listings";
  connection.query(queryString, (err, rows) => {
    if (err) {
      console.log("Failed to fetch listings: " + err);
      res.sendStatus(500);
      res.end();
      return;
    }
    console.log("Successfully fetched property listings...");
    res.json(rows);
  });
});

app.get("/", (req, res) => {
  console.log("Responding to root route...");
  res.send("Hello from route...");
});

// localhost:3001
app.listen(3001, () => {
  console.log("Server is up and running on port 3001...");
});
